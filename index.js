const express = require('express');
const { route } = require('./app/routes/courseRouter.js');
const app = express();
const port = 8000;
const routerCourses = require('./app/routes/courseRouter.js');
const routerReviews = require('./app/routes/reviewRouter.js');
var mongoose = require('mongoose');
var uri = `mongodb://localhost:27017/CRUD_Course`;

const Review = require('./app/models/reviewModel');
const Course = require('./app/models/courseModel');

app.use('/courses', routerCourses);

app.use('/reviews', routerReviews);

mongoose.connect(uri, function (error) {
	if (error) throw error;
	console.log('MongoDB Successfully connected');
})

var firstReview = new Review();
firstReview._id = new mongoose.Types.ObjectId;
firstReview.stars = 1;
firstReview.note = "note";
//firstReview.save();
 
app.listen(port, () => {
  console.log(`App 6.30 listening on port ${port}`);
})
